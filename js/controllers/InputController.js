function initService() {

  /* Get DOM elements */
  var restaurantInput = document.getElementById('restaurant-input');
  var cuisineInput = document.getElementById('cuisine-input');
  var addRestaurantBtn = document.getElementById('add-restaurant-btn');
  var recommendedRestaurant = document.getElementById('googleplaces-div');

  /*Initialize services*/
  var options = { componentRestrictions: {country: "jp"} }
  var autocomplete = new google.maps.places.Autocomplete(restaurantInput, options);
  RestaurantModel.setBounds(new google.maps.LatLngBounds());
  RestaurantModel.setPlacesService(new google.maps.places.PlacesService(recommendedRestaurant));

  /* Initialize Event Listeners */

  autocomplete.addListener('place_changed', function() {
    onPlaceChange(autocomplete.getPlace())
    cuisineInput.focus();
  });

  addRestaurantBtn.addEventListener("click", function() {
    addRestaurant(autocomplete.getPlace(), cuisineInput.value);
  });
}


function clearInput() {
  document.getElementById('restaurant-input').value = ""
  document.getElementById('cuisine-input').value = ""
}

function onPlaceChange(place) {
    if (!place.geometry) {
      window.alert("Invalid place: '" + place.name + "'"); return;
    }
}

function addRestaurant(place, cuisine) {
  if (place == undefined || cuisine == undefined) {return;}
  RestaurantModel.setPlace(place);
  RestaurantModel.setType(cuisine);
  RestaurantModel.add();
  clearInput();
  document.getElementById('input-tags').insertAdjacentHTML('beforeend', getInputTag(place.vicinity, cuisine));

  if (RestaurantModel.getRestaurantInputCount() == 3) {
    RestaurantModel.setSimilarRestaurants();
    setTimeout(function(){
      populateRecommendations(RestaurantModel.getSimilarRestaurants());
    }, 2000);
  }
}

function populateRecommendations(recommendations) {
  var obj = {}
  var tiles = ''
  recommendations.map(places => {
    places.map(recommendation => {
      obj[recommendation.cuisine] = obj[recommendation.cuisine] || []
      obj[recommendation.cuisine] = obj[recommendation.cuisine].concat(recommendation.restaurants)
    });
  });

  console.log(obj);

  for(cuisine in obj) {
    obj[cuisine].sort(function(a, b) {
      return parseFloat(b.rating) - parseFloat(a.rating);
    });
    getRow(cuisine, obj[cuisine]);
  }
}

function getRow(cuisine, restaurants) {
  var row_start = '<div class="stripe">' +
      '<div class="stripe__inner">';
  var row_end = '</div></div>';
  for (var i = 0; i < restaurants.length; i++) {
    row_start += getTile(restaurants[i]);

    if (i == restaurants.length - 1) {
      document.getElementById('recommendations').insertAdjacentHTML('beforeend', row_start + row_end);
    }
  }
}

function getTile(restaurant) {
  return '<a target="_blank" href="https://www.google.co.jp/maps/place/'+restaurant.name+'">' +
    '<div class="tile">' +
      '<div class="tile__media">' +
        '<img class="tile__img" src=" '+ restaurant.photo +' " alt=""  />' +
      '</div>' +
      '<div class="tile__details">' +
        '<div class="tile__title">' + restaurant.name + '</div>' +
          '<p>' + restaurant.address +'</p>' +
          '<p>'+ 'Ratings: ' + restaurant.rating +'</p>' +
      '</div>' +
    '</div>' +
  '</a>';
}

function getInputTag(place, cuisine) {
  return '<div class="alert alert-warning alert-dismissible fade show" role="alert">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
      '<span aria-hidden="true">&times;</span>' +
    '</button>' +
    '<span> '+ cuisine + ' Restaurant' +' </span><br>' +
    '<span>'+ place +'</span>' +
  '</div>'
}

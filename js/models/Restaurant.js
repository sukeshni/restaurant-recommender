var RestaurantModel = (function() {
  let restaurants = [];
  let place;
  let restaurantType;
  let bounds, service;
  let similarRestaurants = [];
  let cuisineTypes = [];
  let defaultImage = 'https://placeholdit.imgix.net/~text?txtsize=33&txt=unavailable&w=350&h=150'

  function fillCuisineTypes() {
    restaurants.map(restaurant => {
      let cuisineType = restaurant.cuisineType;
      if(cuisineTypes.indexOf(cuisineType) === -1)
        cuisineTypes.push(cuisineType)
    });
  }

  function getAverageRestaurantPrice() {
    let totalPrice;
    restaurants.map( restaurant => totalPrice = totalPrice + restaurant.price);
    return totalPrice / restaurants;
  }

  function queryGooglePlacesApi(location) {
    let nearByRestaurantsJsonArray = [];

    cuisineTypes.map (function(cuisine) {
      let nearByRestaurants = [];
      let options = {
        query:  cuisine + ' restaurant',
        location: location,
        radius: '500',
        type: ['restaurant'],
        maxPriceLevel: getAverageRestaurantPrice()
      };

      service.textSearch(options, function(results, status) {
        if(status ===  google.maps.places.PlacesServiceStatus.OK) {
          results.map (function(result){
            var args = {}
            args.lat = result.geometry.location.lat();
            args.lng = result.geometry.location.lng();
            args.priceLevel = result.price_level;
            args.placeId = result.place_id;
            args.name = result.name;
            args.photo = result.photos ? result.photos[0].getUrl({ maxWidth: 640 }) : defaultImage;
            args.rating = result.rating || '-';
            args.address = result.formatted_address
            args.cuisineType = cuisine

            let newRestaurant = new Restaurant(args);
            nearByRestaurants.push(newRestaurant);
          });
        }
      });

      nearByRestaurantsJsonArray.push({
        cuisine: cuisine,
        restaurants: nearByRestaurants
      });
    });
    return nearByRestaurantsJsonArray;
  }

  function getNearByRestaurants(restaurant) {
    let location = new google.maps.LatLng(restaurant.lat, restaurant.lng);

    if(bounds.contains(location)) {
      return null;
    } else {
      bounds.extend(location);
    }
    fillCuisineTypes();
    return queryGooglePlacesApi(location);
  }

  return {
    add: function() {
      var args = {}
      args.lat = place.geometry.location.lat();
      args.lng = place.geometry.location.lng();
      args.priceLevel = place.price_level;
      args.placeId = place.place_id;
      args.name = place.name;
      args.photo = place.photos ? place.photos[0].getUrl({ maxWidth: 640 }) : defaultImage;
      args.url = place.url;
      args.website = place.website;
      args.cuisineType = restaurantType

      let restaurant = new Restaurant(args);
      restaurants.push(restaurant);
    },
    setPlace: function(p) {
      place = p
    },
    setType: function(r) {
      restaurantType = r
    },
    setBounds: function(bound) {
      bounds = bound;
    },
    setPlacesService: function(placesService) {
      service = placesService;
    },
    setSimilarRestaurants: function() {
      restaurants.map(restaurant => similarRestaurants.push(getNearByRestaurants(restaurant)));
    },
    getSimilarRestaurants: function() {
      return similarRestaurants;
    },
    getRestaurantInputCount: function() {
      return restaurants.length;
    }
  }
})();

function Location(lat, lng) {
  this.lat = lat;
  this.lng = lng;
}

function Restaurant(args) {
  Location.call(this, args.lat, args.lng);
  this.priceLevel = args.priceLevel;
  this.placeId = args.placeId;
  this.name = args.name;
  this.photo = args.photo;
  this.rating = args.rating;
  this.mapsUrl = args.url;
  this.website = args.website;
  this.address = args.address;
  this.cuisineType = args.cuisineType;
}
Restaurant.prototype = Object.create(Location.prototype);
